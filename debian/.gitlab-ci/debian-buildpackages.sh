#!/bin/bash

# Exit on error. Append "|| true" if you expect an error.
set -e
# Turn on traces, useful while debugging but commented out by default
#set -x

# Get the version to build from the debian/changelog
version=$(head -n 1 debian/changelog | awk -F'[()]' '{print $2}') 

# Create a local branch if doesn't exists
git checkout origin/mesa-${version} -B mesa-${version} 

# Move to the debian-experimental branch and build de packages
git checkout origin/debian-experimental -B debian-experimental

# The runner has a limited log buffer size, redirect build output to no exceed the limit
gbp buildpackage --git-debian-branch=debian-experimental --git-ignore-new

# As per debug purposes show latest messages in the log
#tail build.log
