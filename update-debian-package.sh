#!/bin/sh

# upstream-experimental is a tracking branch of
# https://gitlab.freedesktop.org/mesa/mesa
git checkout upstream-experimental
git pull

NEWVERSION="19.3+git$(date +%Y%m%d).$(git rev-parse --short=8 HEAD)"

git checkout -B mesa-${NEWVERSION}
git checkout debian-experimental
git rebase mesa-${NEWVERSION}

OLDVERSION=$(head -n 1 debian/changelog | awk -F'[()]' '{print $2}') 

sed -i s/${OLDVERSION}/${NEWVERSION}/g debian/changelog

git add debian/changelog
git commit -s -m "debian: Bump release to latest version from git"

git push eballetbo@gitlab upstream-experimental:refs/heads/upstream-experimental
git push eballetbo@gitlab mesa-${NEWVERSION}:refs/heads/mesa-${NEWVERSION}
git push -f eballetbo@gitlab debian-experimental:refs/heads/debian-experimental

